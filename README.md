# Minimal CI Haskell Test

minimal haskell project to test ci systems with a modern (7.8+) haskell

## Tested CI

* _currently untested_
* ~~desired setup not supported~~
* __supported__

### Cloud

* ~~[circleci](https://circleci.com/)~~
    * no bitbucket support
* ~~[codeship](https://codeship.com)~~
    * out of date haskell (7.4)
* _[wercker](http://wercker.com/)_
* _[shippable](http://www.shippable.com/)_

### Hosted

* _[jenkins](https://jenkins-ci.org/)_
* _[teamcity](https://www.jetbrains.com/teamcity/)_
* _[buildbot](http://buildbot.net/)_
* _[go](http://www.thoughtworks.com/products/go-continuous-delivery)_

# References

[1]: http://www.yegor256.com/2014/10/05/ten-hosted-continuous-integration-services.html
