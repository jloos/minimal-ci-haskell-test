# Codeship CI

Script to build a modern haskell project with Codeship CI

## Plain Build (with Stack)

### Setup

```
cabal update
cabal install cabal-install
cabal install stack
export PATH=~/.cabal/bin:$PATH
stack build
```

fails because of an outdated haskell/ghc:
```
cabal install stack
Resolving dependencies...
cabal: Could not resolve dependencies:
trying: stack-0.0.1
rejecting: base-4.6.0.1/installed-8aa... (conflict: stack => base>=4.7 && <5)
rejecting: base-4.8.0.0, 4.7.0.2, 4.7.0.1, 4.7.0.0, 4.6.0.1, 4.6.0.0, 4.5.1.0,
4.5.0.0, 4.4.1.0, 4.4.0.0, 4.3.1.0, 4.3.0.0, 4.2.0.2, 4.2.0.1, 4.2.0.0,
4.1.0.0, 4.0.0.0, 3.0.3.2, 3.0.3.1 (global constraint requires installed
instance)
```

### Test

### Deploy
